(TeX-add-style-hook
 "crepbook2"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("hyperref" "hidelinks")))
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "graphicx"
    "xcolor"
    "songs"
    "fancybox"
    "hyperref"
    "tikz"
    "licence"
    "xstring"
    "ifthen"
    "framed"
    "currfile")
   (TeX-add-symbols
    '("image" ["argument"] 1)
    '("transposition" 1)
    '("single" 2)
    '("lilypond" 1)
    "l"
    "cover"
    "gtab"
    "Intro"
    "Outro"
    "Bridge"
    "Chorus"
    "Verse"
    "Solo"
    "Pattern"
    "Rythm"
    "Adlib"
    "CB"
    "andname"
    "lastandname"
    "clearheadinfo"
    "subtitle"
    "version"
    "web"
    "mail"
    "email"
    "licence"
    "picture"
    "picturecopyright"
    "footer"
    "lang"
    "lastand"
    "and"
    "songcover"
    "songalbum"
    "songurl"
    "songoriginal"
    "utab"
    "chordtuning"
    "xunit"
    "yunit"
    "musicnoteORIG"
    "textnoteORIG"
    "colbotglue")
   (LaTeX-add-environments
    "tab"
    "bridge"
    "repeatedchords")
   (LaTeX-add-counters
    "auco")
   (LaTeX-add-lengths
    "coverheight"
    "coverspace"
    "capoheight"
    "tablen")
   (LaTeX-add-xcolor-definecolors
    "tango-blue-3"
    "SongbookShade"))
 :latex)

