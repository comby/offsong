(TeX-add-style-hook
 "songs"
 (lambda ()
   (TeX-run-style-hooks
    "etex"
    "keyval"
    "color")
   (TeX-add-symbols
    '("showindex" ["argument"] 2)
    '("idxaltentry" 2)
    '("idxentry" 2)
    '("titleprefixword" 1)
    '("authignoreword" 1)
    '("authbyword" 1)
    '("authsepword" 1)
    '("indexsongsas" 1)
    '("gtabtrans" 2)
    '("newchords" 1)
    '("DeclareFlatSize" 2)
    '("meter" 2)
    '("trchordformat" 2)
    '("notrans" 1)
    '("transposehere" 1)
    '("capo" 1)
    '("transpose" 1)
    '("notenames" 7)
    '("notenamesout" 7)
    '("notenamesin" 7)
    '("shiftdblquotes" 4)
    '("beginscripture" 1)
    '("rep" 1)
    '("musicnote" 1)
    '("textnote" 1)
    '("beginsong" 1)
    '("newsongkey" 2)
    '("foreachtitle" 1)
    '("songpos" 1)
    '("includeonlysongs" 1)
    '("songcolumns" 1)
    '("idxcont" 1)
    '("idxbook" 1)
    '("printchord" 1)
    '("placeversenum" 1)
    '("printversenum" 1)
    '("printsongnum" 1)
    '("placenote" 1)
    '("printscrcite" 1)
    "SB"
    "stitlefont"
    "versefont"
    "notefont"
    "scripturefont"
    "snumbgcolor"
    "notebgcolor"
    "idxbgcolor"
    "snumfgcolor"
    "notefgcolor"
    "idxfgcolor"
    "notejustify"
    "everyverse"
    "everychorus"
    "chordlocals"
    "versesep"
    "afterpreludeskip"
    "beforepostludeskip"
    "baselineadj"
    "clineparams"
    "vvpenalty"
    "ccpenalty"
    "vcpenalty"
    "cvpenalty"
    "brkpenalty"
    "spenalty"
    "songmark"
    "versemark"
    "chorusmark"
    "extendprelude"
    "extendpostlude"
    "idxheadfont"
    "idxtitlefont"
    "idxlyricfont"
    "idxscripfont"
    "idxauthfont"
    "idxrefsfont"
    "minfrets"
    "slides"
    "justifyleft"
    "justifycenter"
    "indexeson"
    "indexesoff"
    "chordson"
    "chordsoff"
    "measureson"
    "measuresoff"
    "scriptureon"
    "scriptureoff"
    "onesongcolumn"
    "twosongcolumns"
    "songlist"
    "nosongnumbers"
    "noversenumbers"
    "repchoruses"
    "norepchoruses"
    "sepverses"
    "commitsongs"
    "nextcol"
    "sclearpage"
    "scleardpage"
    "resettitles"
    "nexttitle"
    "songlicense"
    "setlicense"
    "makeprelude"
    "makepostlude"
    "showauthors"
    "showrefs"
    "scitehere"
    "Acolon"
    "Bcolon"
    "strophe"
    "scripindent"
    "scripoutdent"
    "alphascale"
    "solfedge"
    "prefersharps"
    "preferflats"
    "measurebar"
    "DeclareLyricChar"
    "DeclareNonLyric"
    "DeclareNoHyphen"
    "MultiwordChords"
    "hfil"
    "sharpsymbol"
    "flatsymbol"
    "shrp"
    "flt"
    "ch"
    "mch"
    "memorize"
    "replay"
    "songchapter"
    "songsection"
    "newindex"
    "newscripindex"
    "newauthorindex"
    "indexentry"
    "indextitleentry"
    "songnumstyle"
    "versenumstyle"
    "lastcolglue"
    "lyricfont"
    "chorusfont"
    "versejustify"
    "chorusjustify"
    "placenote"
    "placeversenum"
    "colbotglue"
    "songtitle"
    "songauthors"
    "songcopyright"
    "notenameA"
    "notenameB"
    "notenameC"
    "notenameD"
    "notenameE"
    "notenameF"
    "notenameG"
    "printnoteA"
    "printnoteB"
    "printnoteC"
    "printnoteD"
    "printnoteE"
    "printnoteF"
    "printnoteG"
    "X"
    "O"
    "gtab"
    "hyperlink"
    "v"
    "u"
    "H"
    "t"
    "copyright"
    "songrefs"
    "beginverse"
    "beginchorus"
    "beginscripture"
    "musicnote"
    "textnote"
    "brk"
    "rep"
    "echo"
    "mbar"
    "lrep"
    "rrep"
    "nolyrics")
   (LaTeX-add-environments
    '("idxblock" 1)
    '("songs" 1)
    "song"
    "verse"
    "chorus"
    "songgroup"
    "intersong"
    "scripture"
    "SB")
   (LaTeX-add-counters
    "songnum"
    "versenum")
   (LaTeX-add-lengths
    "idxheadwidth"
    "songnumwidth"
    "versenumwidth"
    "cbarwidth"
    "sbarheight"
    "SB")
   (LaTeX-add-color-definecolors
    "SongbookShade"))
 :latex)

