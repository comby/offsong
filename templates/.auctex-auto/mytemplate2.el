(TeX-add-style-hook
 "mytemplate2"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("babel" "italian" "portuguese" "spanish" "english" "french")))
   (TeX-run-style-hooks
    "tex/chords"
    "pdfpages"
    "inputenc"
    "fontenc"
    "lmodern"
    "babel"))
 :latex)

