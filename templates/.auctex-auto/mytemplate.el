(TeX-add-style-hook
 "mytemplate"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("fontenc" "T1") ("babel" "italian" "portuguese" "spanish" "english" "french") ("geometry" "a4paper" "includeheadfoot" "hmarginratio=1:1" "outer=1.8cm" "vmarginratio=1:1" "bmargin=1.3cm")))
   (TeX-run-style-hooks
    "tex/chords"
    "inputenc"
    "fontenc"
    "lmodern"
    "babel"
    "geometry"))
 :latex)

